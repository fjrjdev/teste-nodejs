import { app } from "./app";
import { Request, Response } from "express";

app.get("/test", (_request: Request, response: Response) => {
  response.status(200).json({
    message: "okay",
  });
});

app.listen(3000, () => {
  console.log("runing server at localhost:3000");
});
